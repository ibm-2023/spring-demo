package com.mywebapp.demo01;

import com.mywebapp.demo01.model.User;
import com.mywebapp.demo01.model.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class Demo01Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo01Application.class, args);
    }

    private static final Logger log = LoggerFactory.getLogger(Demo01Application.class);


//    @Bean
//    public CommandLineRunner demo(UserRepository repository) {
//        return (args) -> {
//            String[][] userData = {
//                    {"Chloe O'Brian", "Chloe@123.com"},
//                    {"Kim Brauer", "Kim@123.com"},
//                    {"David Palmer", "David@123.com"},
//                    {"Michelle Dessler", "Michelle@123.com"}
//            };
//
//            for (String[] data : userData) {
//                User user = new User();
//                user.setName(data[0]);
//                user.setEmail(data[1]);
//                repository.save(user);
//            }
//
//            // fetch all customers
//            log.info("Customers found with findAll():");
//            log.info("-------------------------------");
//            for (User user : repository.findAll()) {
//                log.info(user.toString());
//            }
//            log.info("");
//
//            // fetch an individual customer by ID
//            User user = repository.findById(1L);
//            log.info("Customer found with findById(1L):");
//            log.info("--------------------------------");
//            log.info(user.toString());
//            log.info("");
//
//            // fetch customers by last name
//            log.info("Customer found with findByLastName('Bauer'):");
//            log.info("--------------------------------------------");
//            repository.findByName("Kim Brauer").forEach(bauer -> {
//                log.info(bauer.toString());
//            });
//            log.info("");
//        };
//    }

}
