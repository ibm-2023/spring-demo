package com.mywebapp.demo01.model;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface BusinessRepository extends CrudRepository<User, Integer> {

    List<User> findByName(String name);

    User findById(long id);

}