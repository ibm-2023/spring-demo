package com.mywebapp.demo01.controller;

import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.oidc.user.OidcUser;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.HashMap;
import java.util.Map;

/**
 * Controller for the home page.
 */
@Controller
public class HomeController {
    @GetMapping("/")
    public String home(Model model, @AuthenticationPrincipal OidcUser principal) {
        if (principal != null) { // if the user is authenticated then it will not be null
            model.addAttribute("profile", principal.getClaims());
            // getClaims retrieves associated with the authenticated user, which typically contain user
            // information: name, email, ...
        }
        return "index";
    }

    @GetMapping("/users")
    public String users(Model model, @AuthenticationPrincipal OidcUser principal) {
        return "users";
    }

    @GetMapping("/news")
    public String news(Model model) {
        return "news";
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<JSONObject> getUserById(@PathVariable("id") String id, Model model, @AuthenticationPrincipal OidcUser principal) {
        Map<String, Object> userAttributes = new HashMap<>();
        if (principal != null) {
            userAttributes.put("name", "John Doe");
            userAttributes.put("id", id);
            userAttributes.put("email", "johndoe@example.com");
            JSONObject userJson = new JSONObject(userAttributes);
            return ResponseEntity.ok(userJson);
        }
        userAttributes.put("name", "UNAUTHORIZED");
        userAttributes.put("id", "UNAUTHORIZED");
        userAttributes.put("email", "UNAUTHORIZED");
        JSONObject userJson = new JSONObject(userAttributes);
        return ResponseEntity.ok(userJson);
    }
}
